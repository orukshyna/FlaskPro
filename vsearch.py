def search4letters(phraze: str, letters: str = 'aueoi') -> set:
    return set(letters).intersection(set(phraze))

def search4vowels(phraze: str) -> set:
    return set('aeoiu').intersection(set(phraze))
