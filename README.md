# _FlaskPro_

### Intro
_FlaskPro_ is built based on the resources from the book: 
"Head First Python: A Brain-Friendly Guide" authored by Paul Barry 
Here is a link to Amazon store https://www.amazon.com/Paul-Barry/e/B001HMX52K/ref=dp_byline_cont_pop_book_1
- FlaskPro includes: 
* generators part with recommendations how to organize code better;
* the main file vsearch4web.py with the search, user login/logout, viewlog logic;
* related modules: DBcm with database implementation, checker with check_logged_in decorator function;
* related folders: templates and static for frontend part of application.

#### Setup
for venv installation used https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/
* ```py -m venv env```

for installing MySQL used https://dev.mysql.com/downloads/installer/
* ```(mysql-installer-community-8.0.24.0.msi)``` - full distribution (in case of no internet connection);
* default configuration;
* add DBAdmin user with credentials
* add PATH to MySQL Server on Windows: https://dev.mysql.com/doc/mysql-windows-excerpt/5.7/en/mysql-installation-windows-path.html

for using MySQL:
* login: ```mysql -u root -p```
* to create DB ```create database vsearchlogDB;```
  
* to enter DB use ```use <db_name>;```
* to create table:
    ```create table log (
    -> id int auto_increment primary key,
    -> ts timestamp default current_timestamp,
    -> phrase varchar(128) not null,
    -> letters varchar(32) not null,
    -> ip varchar(16) not null,
    -> browser_string varchar(256) not null,
    -> results varchar(64) not null );```
  
* to view structure of table 'log' ```describe log;```
* to view all recordings in table ```select * from log;```
* to quit in terminal: ```quit```

### Run
start the app from Windows:
```py -3 vsearch4web.py```

start app from Linux, Mac OS:
``` python3 vsearch4web.py```

main page: 
```http://127.0.0.1:5000/```

fill the Phrase field:

![img_3.png](static/img_3.png)

click the button 'Do it!' to search the results for the filled Phrase:

![img_2.png](static/img_2.png)

view the page with logs (but first need to login):
```http://127.0.0.1:5000/viewlog```

![img_1.png](static/img_1.png)