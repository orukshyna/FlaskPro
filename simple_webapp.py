from flask import Flask, session

from checker import check_logged_in

app = Flask(__name__)

app.secret_key = 'SecretKey'


@app.route('/')
def landing() -> str:
    landing_content = ('<h2>Hello from the simple webapp! </h2>' 
                       '<p>Navigate to another pages with the links: </p>'   
                       '<a href="http://127.0.0.1:5000/page_1">Page one</a>'
                       ' or ' 
                       '<a href="http://127.0.0.1:5000/page_2">Page two</a>')
    return landing_content

@app.route('/page_1')
@check_logged_in
def page_1() -> str:
    return 'This is page number 1.'

@app.route('/page_2')
@check_logged_in
def page_2() -> str:
    return 'This is page number 2.'

@app.route('/login')
def do_login() -> str:
    session['logged_in'] = True
    return 'User is logged in.'

@app.route('/logout')
def do_logout() -> str:
    session.pop('logged_in')
    return 'User is logged out.'


if __name__ == '__main__':
    app.run(debug=True)
