import requests

def generators_from_urls(urls: tuple) -> tuple:
    for response in (requests.get(url) for url in urls):
        #  yield - to continue return other urls
        yield len(response.content), response.status_code, response.url
