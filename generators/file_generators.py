import csv

with open('timetable.csv') as file:
    line = [line for line in csv.DictReader(file)]
    print(line, end='\n\n')


from datetime import datetime
import pprint


def convert2ampm(time24: str) -> str:
    return datetime.strptime(time24, '%H:%M').strftime('%I:%M%p')


with open('timetable.csv') as file:
    file.readline()  # read the head of the file to skip it
    flights = {}
    for line in file:
        time, destination = line.strip().split(',')
        flights[time] = destination

print(flights, end='\n\n')

flights_v2 = {}
for time, destination in flights.items():
    flights_v2[convert2ampm(time)] = destination.title()

# the alternative with generators ⤵️

flights_v2 = {convert2ampm(time): destination.title()
              for time, destination in flights.items()}

pprint.pprint(flights_v2)

flights_v3 = {}
for destination in flights_v2.values():
    flights_v3[destination] = [time for time, dest
                               in flights_v2.items() if dest == destination]

# interesting research in a book it is suggested
# to use set(flights_v2.values()) to avoid duplicates,
# but it has the same result without set. why?
# because keys in dict are unique.
# Dear reader, you could try
# to add one more row in csv file with the same time, but different city -
# and observe the magic ✨

# the alternative with generators ⤵️

flights_v3 = {destination: [time for time, dest
                            in flights_v2.items() if dest == destination]
              #  when I wrote about set I intended the part:
              #  'for destination in set(flights_v2.values())
              for destination in flights_v2.values()}

pprint.pprint(flights_v3)

print('\n\n', 'Generator for set:', end='\n\n')

letters = {'a', 'e', 'i', 'o', 'u'}
message = 'This is a message for interested persons 🕵️️'
found = set()
for letter in letters:
    if letter in message:
        found.add(letter)

#  the statement using generators:
found_generator = {letter for letter in letters
                   if letter in message}

if found == found_generator:
    print('found and found_generator are equal:',
          found_generator, sep='\n', end='\n\n')
else:
    print('found and found_generator are different:',
          found, found_generator, sep='\n', end='\n\n')


import requests

urls = ('https://talkpython.fm/',
        'https://www.pylint.org/',
        'https://wiki.python.org/moin/WebFrameworks')

for response in [requests.get(url) for url in urls]:
    print(len(response.content), response.status_code, response.url, sep=' ➡ ')

#  the difference between ⬆️and ⬇️is in generator-statement ⬇️
#  showing results one by one when got it:
for response in (requests.get(url) for url in urls):
    print(len(response.content), response.status_code, response.url, sep=' ➡ ')


from url_utils import generators_from_urls


urls = ('https://talkpython.fm/',
        'https://www.pylint.org/',
        'https://wiki.python.org/moin/WebFrameworks')

for response_length, status, url in generators_from_urls(urls):
    print(response_length, status, url, sep=' ➡ ')


# generator of dictionary has the view:
urls_response = {url: size for size, _, url
                 in generators_from_urls(urls)}

pprint.pprint(urls_response)
