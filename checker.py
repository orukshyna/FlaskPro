from flask import session

from functools import wraps


def check_logged_in(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if 'logged_in' not in session:
            return 'User is logged Out currently.'
        return func(*args, **kwargs)
    return wrapper
