from flask import (Flask, render_template, request,
                   session, copy_current_request_context)
from threading import Thread
import os
from dotenv import load_dotenv

from vsearch import search4letters
from DBcm import UseDatabase, ConnectionError, CredentialsError, SQLError
from checker import check_logged_in

load_dotenv()
app = Flask(__name__)
app.secret_key = os.getenv('SECRET_KEY')
app.config['dbconfig'] = {'host': '127.0.0.1',
                          'user': os.getenv('USER'),
                          'password': os.getenv('PASSWORD'),
                          'database': 'vsearchlogDB',}


@app.route('/login')
def do_login() -> str:
    session['logged_in'] = True
    return 'User is logged in.'

@app.route('/logout')
def do_logout() -> str:
    session.pop('logged_in')
    return 'User is logged out.'

@app.route('/search', methods=['POST'])
def do_search() -> 'html':
    @copy_current_request_context
    def log_request(req: 'flask_request', res: str) -> None:
        with UseDatabase(app.config['dbconfig']) as cursor:
            _SQL = """insert into log
                    (phrase, letters, ip, browser_string, results)
                    values
                    (%s, %s, %s, %s, %s)"""
            cursor.execute(_SQL, (req.form['phrase'],
                                  req.form['letters'],
                                  req.remote_addr,
                                  req.user_agent.browser,
                                  res,))

    phrase = request.form['phrase']
    letters = request.form['letters']
    title = 'Here are your results:'
    results = str(search4letters(phrase, letters))
    try:
        Thread(target=log_request, args=(request, results)).start()
    except Exception as error:
        return f'Logging failed with error: {error}'
    return render_template('results.html', 
                           the_title=title,
                           the_results=results,
                           the_phrase=phrase,
                           the_letters=letters)

@app.route('/')
@app.route('/entry')
def entry_page() -> 'html':
    return render_template('entry.html', 
                           the_title='Welcome to search4letters on web! =)')

@app.route('/viewlog')
@check_logged_in
def view_the_log() -> 'html':
    try:
        with UseDatabase(app.config['dbconfig']) as cursor:
            _SQL = """select phrase, letters, ip, browser_string, results from log"""
            cursor.execute(_SQL)
            contents = cursor.fetchall()
        titles = ('Phrase', 'Letters', 'Remote_addr', 'User_agent', 'Results')
        return render_template('viewlog.html',
                               the_title='View Log',
                               the_row_titles=titles,
                               the_data=contents,)
    except ConnectionError as error:
        print(f'Check the DB is connected. Connection failed with: {error}')
    except CredentialsError as error:
        print(f'Check user_id and password. Connection failed with: {error}')
    except SQLError as error:
        print(f'Check the query. Details: {error}')
    except Exception as error:
        print(f'Error occurred. Details: {error}')
    return 'Error'


if __name__ == '__main__':
    app.run(debug=True)
